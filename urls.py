from django.conf.urls import patterns, include, url
from django.conf import settings

#views
from django.views.generic import TemplateView

from views import Home

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# filebrowser
from filebrowser.sites import site

# statics
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^$', Home.as_view(), {}, "home_view"),
    
    url(r'^dossier/', include('dossiers.urls')),
    url(r'^infos/', include('articles.urls')),
    url(r'^tags/', include('tags.urls')),
    url(r'^medias/', include('medias.urls')),

)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG: 
    urlpatterns += patterns('',
                            url(r'^medias/(?P<path>.*)/?$' , 'django.views.static.serve', { 'document_root': settings.MEDIA_ROOT, }),
                            (r'^404$', TemplateView.as_view(template_name="404.html")),
                            (r'^500$', TemplateView.as_view(template_name="500.html")),
                            )