

// ---------------------------------------------------------------------------------------- Scroll home
$(function(){

    $body = $('body');
    if ($body.hasClass('dossier') || $body.hasClass('article') || $body.hasClass('media') || window.location.hash == '#dossiers' ){
        $top = $('#header').offset().top - 50 ;
        $body.scrollTo( $top, 200 );
    };

    $(window).scroll(function() {
        if($(window).scrollTop() > 100){
            $body.addClass('scrolled');
        } else {
            $body.removeClass('scrolled')
        }
    });

    // ----------------------------------------------------------------------------- meta
    $('.meta').each(function(){
        $(this).attr('data-closable', false)
    })
    $('.meta').on('click', function(){
        $this = $(this);
        $span = $this.find('span');
        $this.addClass('active');
        var closable = $this.attr('data-closable');
        var $close = $('<em class="close"/>');
        
        if(closable == 'false') {
            $span.append($close);
            $this.attr('data-closable', true)
        };

        $close.click(function(e){
            $this.removeClass('active');
            e.stopPropagation();
            $(this).remove();
            $this.attr('data-closable', false);
            $span.css({left:'-5px', top:'-5px'})
        })
        

        
    })
    $('.meta span').drags();


    // ----------------------------------------------------------------------------- gallery slideshow
    function slideSwitch() {
        var $active = $('.slideshow .active');
        if ( $active.length == 0 ) $active = $('.slideshow div:last');
        var $next =  $active.next().length ? $active.next() : $('.slideshow div:first');
        $active.removeClass('active');
        $next.addClass('active');
    }

    $body.on('click', '.slideshow div', function(){
        slideSwitch();
    })
})


// ---------------------------------------------------------------------------------------- Map 


var map;
document.map;


function initializeMap() {

    L.mapbox.accessToken = 'pk.eyJ1IjoianVsaWVuYiIsImEiOiJWTzBDVFg0In0.9nmlzbnQ2mMOUX4Tu0ELzQ';
    
    var map = L.mapbox.map('map_canvas', 'julienb.2a03ae03');
    
    map.setView([43.295, -0.370], 16);

    
    
    map.scrollWheelZoom.disable();

    all_markers=[]
    for(var i=0; i<document.collections.length; i++){
        
        setMarkers(map, document.collections[i]);
        // adaptation du niveau de zoom aux marqueurs
        all_markers = all_markers.concat(document.collections[i].markers);
    }


}

function createMarker(marker, map, index, collection){
     
    collection.markers[index] = new L.marker([marker.latlng.split(',')[0], marker.latlng.split(',')[1]], {
        icon: new L.Icon({iconUrl: collection.icon}),
        title: marker.texte
    }).addTo(map);
    
    collection.markers[index].on('mouseover', function (e) {
        $('#title').html('<strong>' + marker.title + '</strong>' + '<span>' + marker.texte + '</span>' );
        $('#title').css('display', 'block');
        
    });

    collection.markers[index].on('mouseout', function (e) {
        $('#title').fadeOut();       
    });

    collection.markers[index].on('click', function (e) {
        window.location.href = marker.url;
    });

    return collection.markers[index];
}



function setMarkers(map, collection) {
    for (var i = 0; i < collection.items.length; i++) {
        createMarker(collection.items[i], map, i, collection);
    }
}

// init
$(window).bind('load', function () {
    initializeMap();
});




