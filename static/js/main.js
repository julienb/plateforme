

// ---------------------------------------------------------------------------------------- Scroll home
$(function(){

    $body = $('body');
    if ($body.hasClass('dossier') || $body.hasClass('article') || $body.hasClass('media') || window.location.hash == '#dossiers' ){
        $top = $('#header').offset().top - 50 ;
        $body.scrollTo( $top, 200 );
    };

    $(window).scroll(function() {
        if($(window).scrollTop() > 100){
            $body.addClass('scrolled');
        } else {
            $body.removeClass('scrolled')
        }
    });

    // ----------------------------------------------------------------------------- meta
    $('.meta').each(function(){
        $(this).attr('data-closable', false)
    })
    $('.meta').on('click', function(){
        $this = $(this);
        $span = $this.find('span');
        $this.addClass('active');
        var closable = $this.attr('data-closable');
        var $close = $('<em class="close"/>');
        
        if(closable == 'false') {
            $span.append($close);
            $this.attr('data-closable', true)
        };

        $close.click(function(e){
            $this.removeClass('active');
            e.stopPropagation();
            $(this).remove();
            $this.attr('data-closable', false);
            $span.css({left:'-5px', top:'-5px'})
        })
        

        
    })
    $('.meta span').drags();


    // ----------------------------------------------------------------------------- gallery slideshow
    function slideSwitch() {
        var $active = $('.slideshow .active');
        if ( $active.length == 0 ) $active = $('.slideshow div:last');
        var $next =  $active.next().length ? $active.next() : $('.slideshow div:first');
        $active.removeClass('active');
        $next.addClass('active');
    }

    $body.on('click', '.slideshow div', function(){
        slideSwitch();
        alert('eee')
    })
})


// ---------------------------------------------------------------------------------------- Map 
var ib = new InfoBox();
var map;
document.map;
var bound = new google.maps.LatLngBounds();



function initializeMap() {
    var stylez = [{     
        "elementType": "labels.text.fill",     "stylers": [       { "visibility": "on" },       { "color": "#ffffff" }     ]   },{     "featureType": "administrative",     "elementType": "geometry",     "stylers": [       { "visibility": "off" }     ]   },{     "featureType": "poi",     "stylers": [       { "visibility": "off" }     ]   },{     "featureType": "transit",     "stylers": [       { "visibility": "off" }     ]   },{     "featureType": "water",     "stylers": [       { "color": "#222222" }     ]   },{     "featureType": "landscape",     "stylers": [       { "saturation": 0 },       { "hue": "#000000" },       { "gamma": 0.7 },       { "lightness": -90 }     ]   },  {     "featureType": "road",     "elementType": "geometry.stroke",     "stylers": [       { "visibility": "off" }     ]   },{     "featureType": "road",     "elementType": "geometry.fill",     "stylers": [       { "weight": 0.4 },       { "hue": "#fff" },       { "lightness": 100 }     ]   },{     "elementType": "labels",     "stylers": [       { "visibility": "off" }     ]   },{     "elementType": "labels.text.stroke",     "stylers": [       { "visibility": "off" }     ]   
    }];
    //var stylez = [ { "featureType": "water", "stylers": [ { "visibility": "on" }, { "color": "#ffffff" } ] },{ "featureType": "landscape", "stylers": [ { "visibility": "on" }, { "color": "#ff8001" }, { "lightness": 96 } ] },{ "featureType": "road", "stylers": [ { "visibility": "on" }, { "color": "#ff8200" }, { "weight": 0.5 } ] },{ },{ "elementType": "labels", "stylers": [ { "visibility": "off" } ] },{ "featureType": "poi", "stylers": [ { "color": "#ff5c00" }, { "lightness": 97 } ] },{ "featureType": "transit", "stylers": [ { "color": "#ff8000" }, { "lightness": 66 } ] },{ "featureType": "administrative", "stylers": [ { "visibility": "off" } ] } ] 

    var mapOptions = {
        center: new google.maps.LatLng(43.295, -0.370), 
        zoom: 18, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: stylez,
        scrollwheel: false,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL
        }
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    
    google.maps.event.addListener(map, "click", function() { ib.close() });
    
    
    // infowindow = new google.maps.InfoWindow({
    //     content: "loading..."
    // });
    all_markers=[]
    for(var i=0; i<document.collections.length; i++){
        
        setMarkers(map, document.collections[i]);
        // adaptation du niveau de zoom aux marqueurs
        all_markers = all_markers.concat(document.collections[i].markers);
    }
    
    for(var i in all_markers) {
        bound.extend(all_markers[i].getPosition());
    }
    map.fitBounds(bound);



}

function createMarker(marker, map, index, collection){
     
    var siteLatLng = new google.maps.LatLng(marker.latlng.split(',')[0], marker.latlng.split(',')[1]);

    collection.markers[index] = new google.maps.Marker({
        position: siteLatLng,
        map: map,
        //title: marker.title,
        zIndex: marker.index,
        html: marker.texte,
        icon: collection.icon
    });

    // mode infobox
    var boxText = document.createElement("div");
    boxText.style.cssText = "margin-top: -100px; height:100px; background: white; padding: 10px 30px 0px 10px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;";
    boxText.innerHTML = '<strong>' + marker.title + '</strong><br>' + '<span>' + marker.texte + '</span>';



    var myOptions = {
             content: boxText
            ,disableAutoPan: false
            ,maxWidth: 0
            ,zIndex: null
            ,boxStyle: { 
              background: "url('http://ateliers.esapyrenees.fr/visages/img/box-marker.png') no-repeat 2px 0px"
              ,width: "295px", margin:"0 0 20px -96px"
             }
            ,closeBoxMargin: "-85px 12px 100px 2px"
            ,closeBoxURL: document.STATIC_URL + "img/close.png"
            ,infoBoxClearance: new google.maps.Size(0, 120)
            ,isHidden: false
            ,alignBottom: true
            ,pane: "floatPane"
            ,enableEventPropagation: false
    };

    google.maps.event.addListener(collection.markers[index], "mouseover", function (e) {
        //console.log('hoever')
        //ib.close();
        //ib.setOptions(myOptions);
        //ib.open(map, this);
        //map.setCenter(collection.markers[index].getPosition());
        $('#title').html('<strong>' + marker.title + '</strong>' + '<span>' + marker.texte + '</span>' );
        $('#title').css('display', 'block');
        
    });

    google.maps.event.addListener(collection.markers[index], "mouseout", function (e) {
        $('#title').fadeOut();
        ib.close();
    });

    google.maps.event.addListener(collection.markers[index], "click", function (e) {
        window.location.href = marker.url;
    });

    return collection.markers[index];
}



function setMarkers(map, collection) {
    for (var i = 0; i < collection.items.length; i++) {
        createMarker(collection.items[i], map, i, collection);
        collection.visibility = true;
        if(i==0 && collection.control==false){
            var controlDiv = document.createElement('div');
            var control = new customControl(controlDiv, map, collection);
            controlDiv.index = 8;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);  
            collection.control=true;
        }
    }
}

function customControl(controlDiv, map, collection) {

    // Set CSS styles for the DIV containing the control
    // Setting padding to 5 px will offset the control
    // from the edge of the map.
    controlDiv.style.padding = '5px';

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '2px';
    controlUI.style.textTransform = 'uppercase';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to set the map to Home';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.innerHTML = collection.title;
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    google.maps.event.addDomListener(controlUI, 'click', function() {
        if (collection.visibility == true) {
            for (var i = 0; i < collection.items.length; i++) {
                collection.markers[i].setMap(null);
            };
            this.style.opacity = .4;
            collection.visibility = false;
        } else {
            this.style.opacity = 1;
            setMarkers(map, collection);
        }
        
    });
}


// init
//google.maps.event.addDomListener(window, 'load', initializeMap);

