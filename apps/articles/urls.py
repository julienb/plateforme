#-*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from models import Article
from views import *

urlpatterns = patterns('',
   url(r'^(?P<pk>\d+)/(?P<slug>\S+)$', ArticleDetail.as_view(), {}, "article_detail_view"),
   url(r'^$', ArticleList.as_view(), {}, "article_list_view"),      
                       
)