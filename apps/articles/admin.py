#-*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from articles.models import Article

from sorl.thumbnail.admin import AdminImageMixin
from django.db import models
from django.forms.widgets import HiddenInput


class ArticleAdmin(admin.ModelAdmin):   
    
    class Media:
        js = (
              settings.STATIC_URL+'grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
              settings.STATIC_URL+'tiny_mce_setup/tiny_mce_setup.js',
              )

    date_hierarchy = 'publication_date'

    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title', 'short_title', 'authors', 'introduction', 'text')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-open',),
            'fields':( 'status', 'publication_date')
            }),
        )
    
    ordering = ('-publication_date',)
    search_fields = ('title', 'introduction',)
    list_editable = ('status',)
    list_display_links = ('title', 'id',)

    autocomplete_search_fields =('title', 'id',)
    
    raw_id_fields =  ('authors',)
    autocomplete_lookup_fields = {
        'm2m': ['authors',],
    }
    
    
admin.site.register(Article, ArticleAdmin)



    