#-*- coding: utf-8 -*-

from django.views.generic import DetailView, ListView
from datetime import datetime,timedelta

from articles.models import PublicArticle
from dossiers.models import PublicDossier


# event detail view    
class ArticleDetail(DetailView):
    model = PublicArticle
    template_name = "article.html"

    def get_context_data(self, **kwargs):

        context = super(ArticleDetail, self).get_context_data(**kwargs)
        context['dossiers'] = PublicDossier.objects.all()
        context['articles'] = PublicArticle.objects.all()    

        return context


class ArticleList(ListView):
    model = PublicArticle
    template_name = "article.html"

    def get_context_data(self, **kwargs):

        context = super(ArticleList, self).get_context_data(**kwargs)
        context['dossiers'] = PublicDossier.objects.all()
        context['object'] = PublicArticle.objects.filter(title__icontains='Introduction')[0]

        context['articles'] = PublicArticle.objects.all()    

        return context
