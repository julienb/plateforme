#-*- coding: utf-8 -*- 

from django.db import models
from django.db.models import permalink
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core import urlresolvers

from model_utils.managers import QueryManager
from model_utils import Choices

from ag.utils.slughifi import slughifi

from filebrowser.fields import FileBrowseField
from datetime import datetime
from time import strftime

from tags.models import TaggedContent, TaggedDossier
from taggit.managers import TaggableManager





class Article(models.Model):
    
    title = models.CharField(max_length=128, verbose_name='Titre', help_text='')    
    short_title = models.CharField(max_length=128, verbose_name='Titre court', help_text='', null=True, blank=True, )    
    authors = models.ManyToManyField(User, verbose_name=u'Auteurs', related_name='article_authors', null=True, blank=True, )
    introduction = models.TextField(verbose_name='Introduction', null=True, blank=True, )
    text = models.TextField(verbose_name='Texte', null=True, blank=True)

    status = models.CharField(choices=settings.EDITORIAL_STATUS, max_length=24, default='PUBLISHED', verbose_name = 'Statut', help_text='Publié, brouillon…')
    publication_date = models.DateTimeField('Date', default=datetime.now, blank=True)

    
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Article"
        verbose_name_plural = "Articles"
    
    def __unicode__(self):
        return self.title

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)
    
    @permalink
    def get_absolute_url(self):
        return ('article_detail_view',(),{'pk':self.id, 'slug':slughifi(self.title)})

    def get_admin_url(self):
        print self.__dict__
        content_type = ContentType.objects.get(model="Article")
        object_admin_url = urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return object_admin_url
  
    
class PublicArticle(Article):
    objects = QueryManager(status='PUBLISHED', publication_date__lte=datetime.now).order_by('publication_date')
    class Meta:
        proxy = True       



