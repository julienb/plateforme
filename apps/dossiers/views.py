#-*- coding: utf-8 -*-

from django.views.generic import DetailView, ListView
from datetime import datetime,timedelta

from dossiers.models import PublicDossier
from medias.models import Media, PublicMedia

from model_utils.models import InheritanceCastModel


# event detail view    
class DossierDetail(DetailView):
    model = PublicDossier
    template_name = "dossier.html"


    
    def get_context_data(self, **kwargs):

        context = super(DossierDetail, self).get_context_data(**kwargs)
        context['dossiers'] = PublicDossier.objects.all()    
        context['medias'] = PublicMedia.objects.filter(dossier=self.object)

        related_dossiers = PublicDossier.objects.filter(
            tags__name__in=list(self.object.tags.values_list('name', flat=True))
        ).exclude(id=self.object.id)
        context['related_dossiers'] = related_dossiers
        
        context['mappables_medias'] = PublicMedia.objects.filter(dossier=self.object, real_type__model__in=['Sound', 'Video', 'Photo'])
            

        return context
