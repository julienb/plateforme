#-*- coding: utf-8 -*- 

from django.db import models
from django.db.models import permalink
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core import urlresolvers

from model_utils.managers import QueryManager
from model_utils import Choices

from ag.utils.slughifi import slughifi

from filebrowser.fields import FileBrowseField
from datetime import datetime
from time import strftime

from tags.models import TaggedContent, TaggedDossier
from taggit.managers import TaggableManager





class Dossier(models.Model):
    
    title = models.CharField(max_length=128, verbose_name='Titre', help_text='')    
    authors = models.ManyToManyField(User, verbose_name=u'Auteurs', related_name='dossier_authors')
    introduction = models.TextField(verbose_name='Introduction', help_text='Maximum 500 caractères')
    image = FileBrowseField("Image", max_length=500, extensions=[".jpg",".png"], help_text='Uploader et sélectionner une image de grande taille (> 800px)')
    text = models.TextField(verbose_name='Texte', null=True, blank=True, help_text='Texte complet, issu de votre PDF')

    location = models.TextField(verbose_name='Espace géographique', null=True, blank=True, help_text='Quartier, ville, rue, lieu. Par exemple : « Quartier du Hédas »')

    pdf = FileBrowseField("PDF", max_length=500, extensions=[".pdf",], help_text='Uploader et sélectionner le document pdf final du dossier.')

    status = models.CharField(choices=settings.EDITORIAL_STATUS, max_length=24, default='PUBLISHED', verbose_name = 'Statut', help_text='Publié, brouillon…')
    publication_date = models.DateTimeField('Date', default=datetime.now, blank=True)

    tags = TaggableManager(blank=True, through=TaggedDossier)    
    
    latlng = models.CharField(verbose_name ='Latitude / longitude', blank=True, max_length=100)

    
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Dossier"
        verbose_name_plural = "Dossiers"
    
    def __unicode__(self):
        return self.title

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)
    
    @permalink
    def get_absolute_url(self):
        return ('dossier_detail_view',(),{'pk':self.id, 'slug':slughifi(self.title)})

    def get_admin_url(self):
        content_type = ContentType.objects.get(model="Dossier")
        object_admin_url = urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return object_admin_url
  
    
class PublicDossier(Dossier):
    objects = QueryManager(status='PUBLISHED', publication_date__lte=datetime.now).order_by('publication_date')
    class Meta:
        proxy = True       



