#-*- coding: utf-8 -*-

from django.conf.urls import patterns
from models import Dossier
from views import *

urlpatterns = patterns('',
   (r'^(?P<pk>\d+)/(?P<slug>\S+)$', DossierDetail.as_view(), {}, "dossier_detail_view"),
                       
)