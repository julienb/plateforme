from django import forms
from dossiers.models import Dossier
from medias.widgets import LocationWidget
 
class DossierForm(forms.ModelForm):
    latlng = forms.CharField(widget=LocationWidget()) # http://djangosnippets.org/snippets/2106/
    class Meta:
        model = Dossier                        
