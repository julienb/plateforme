#-*- coding: utf-8 -*-


from taggit.models import Tag
from django.utils import simplejson
from django.http import HttpResponse
from django.views.generic import DetailView, ListView
from medias.models import Media
from dossiers.models import Dossier, PublicDossier

from datetime import datetime

from itertools import chain


class TagDetail(DetailView):
    model = Tag
    template_name = "tag.html"

    
    def get_context_data(self, **kwargs):

        

        context = super(TagDetail, self).get_context_data(**kwargs)
        print self.__dict__
        context['all_dossiers'] = PublicDossier.objects.all()
        context['tags'] = Tag.objects.all().order_by('name')
        context['medias'] = Media.objects.filter(taggedcontent__tag=self.object, 
                                                            status="published",
                                                            publication_date__lte=datetime.now
                                                            ).order_by('-publication_date')
        context['dossiers'] = Dossier.objects.filter(taggeddossier__tag=self.object, 
                                                            status="published",
                                                            publication_date__lte=datetime.now
                                                            ).order_by('-publication_date')
        
        return context



class TagList(ListView):
    model = Tag
    template_name = "tags.html"

    def get_context_data(self, **kwargs):
        context = super(TagList, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.all().order_by('name')
        return context        

