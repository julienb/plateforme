#-*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from tags.views import TagDetail, TagList

urlpatterns = patterns('',
    url(r'^(?P<pk>\d+)/(?P<slug>\S+)$',TagDetail.as_view(), {}, "tag_detail_view"),
    url(r'^$', TagList.as_view(), {}, "tag_list_view"),                       
)