from taggit.models import TaggedItemBase
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

class TaggedContent(TaggedItemBase):
    """
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    """

    content_object = models.ForeignKey('medias.Media')


class TaggedDossier(TaggedItemBase):
    """
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    """

    content_object = models.ForeignKey('dossiers.Dossier')
    