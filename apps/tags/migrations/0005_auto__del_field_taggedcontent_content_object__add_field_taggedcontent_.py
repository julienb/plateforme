# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'TaggedContent.content_object'
        db.delete_column('tags_taggedcontent', 'content_object_id')

        # Adding field 'TaggedContent.content_type'
        db.add_column('tags_taggedcontent', 'content_type',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=11, to=orm['contenttypes.ContentType']),
                      keep_default=False)

        # Adding field 'TaggedContent.object_id'
        db.add_column('tags_taggedcontent', 'object_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'TaggedContent.content_object'
        raise RuntimeError("Cannot reverse this migration. 'TaggedContent.content_object' and its values cannot be restored.")
        # Deleting field 'TaggedContent.content_type'
        db.delete_column('tags_taggedcontent', 'content_type_id')

        # Deleting field 'TaggedContent.object_id'
        db.delete_column('tags_taggedcontent', 'object_id')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'tags.taggedcontent': {
            'Meta': {'object_name': 'TaggedContent'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tags_taggedcontent_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['tags']