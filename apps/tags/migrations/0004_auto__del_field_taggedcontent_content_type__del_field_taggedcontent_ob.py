# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'TaggedContent.content_type'
        db.delete_column('tags_taggedcontent', 'content_type_id')

        # Deleting field 'TaggedContent.object_id'
        db.delete_column('tags_taggedcontent', 'object_id')

        # Adding field 'TaggedContent.content_object'
        db.add_column('tags_taggedcontent', 'content_object',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['medias.Media']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'TaggedContent.content_type'
        db.add_column('tags_taggedcontent', 'content_type',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['contenttypes.ContentType']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'TaggedContent.object_id'
        raise RuntimeError("Cannot reverse this migration. 'TaggedContent.object_id' and its values cannot be restored.")
        # Deleting field 'TaggedContent.content_object'
        db.delete_column('tags_taggedcontent', 'content_object_id')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'medias.media': {
            'Meta': {'object_name': 'Media'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'real_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'tags.taggedcontent': {
            'Meta': {'object_name': 'TaggedContent'},
            'content_object': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['medias.Media']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tags_taggedcontent_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['tags']