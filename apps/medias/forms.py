from django import forms
from medias.models import Photo, Video, Sound, Text, Link, Book, Map, Gallery, Image
from medias.widgets import LocationWidget
 
class PhotoForm(forms.ModelForm):
    latlng = forms.CharField(widget=LocationWidget()) # http://djangosnippets.org/snippets/2106/
    class Meta:
        model = Photo

class VideoForm(forms.ModelForm):
    latlng = forms.CharField(widget=LocationWidget()) # http://djangosnippets.org/snippets/2106/
    class Meta:
        model = Video        

class SoundForm(forms.ModelForm):
    latlng = forms.CharField(widget=LocationWidget()) # http://djangosnippets.org/snippets/2106/
    class Meta:
        model = Sound

class ImageForm(forms.ModelForm):
    latlng = forms.CharField(widget=LocationWidget()) # http://djangosnippets.org/snippets/2106/
    class Meta:
        model = Image                        

class InlineImageForm (forms.ModelForm):
    title= forms.CharField()
    latlng = forms.CharField(widget=LocationWidget()) # http://djangosnippets.org/snippets/2106/
    #order = PositiveSmallIntegerField()

    class Meta:
        model = Image                        