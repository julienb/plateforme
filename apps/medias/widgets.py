from django import forms
from django.db import models
from django.utils.safestring import mark_safe

DEFAULT_WIDTH = 450
DEFAULT_HEIGHT = 200

DEFAULT_LAT = 43.296995
DEFAULT_LNG = -0.367999

class LocationWidget(forms.TextInput):
    def __init__(self, *args, **kw):

        self.map_width = kw.get("map_width", DEFAULT_WIDTH)
        self.map_height = kw.get("map_height", DEFAULT_HEIGHT)

        super(LocationWidget, self).__init__(*args, **kw)
        self.inner_widget = forms.widgets.HiddenInput()

    def render(self, name, value, *args, **kwargs):
        
        if value is None or value is u'' :
            lat, lng = DEFAULT_LAT, DEFAULT_LNG
        else:
            if isinstance(value, unicode):
                a, b = value.split(',')
            else:
                a, b = value
            lat, lng = float(a), float(b)

#        print lat, lng, name
        jsname = name.replace('-', '_')
        js = '''
<script>
<!--
    var map_%(jsname)s;
    
    function savePosition_%(jsname)s(point)
    {
        var input = document.getElementById("id_%(name)s");
        input.value = point.lat().toFixed(6) + "," + point.lng().toFixed(6);
        map_%(jsname)s.panTo(point);
    }
    
    function load_%(jsname)s() {
        var point = new google.maps.LatLng(%(lat)f, %(lng)f);

        var options = {
            zoom: 16,
            center: point,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            // mapTypeControl: true,
            // navigationControl: true
        };
        
        map_%(jsname)s = new google.maps.Map(document.getElementById("map_%(jsname)s"), options);

        var marker = new google.maps.Marker({
                map: map_%(jsname)s,
                position: new google.maps.LatLng(%(lat)f, %(lng)f),
                draggable: true
        
        });
        google.maps.event.addListener(marker, 'dragend', function(mouseEvent) {
            savePosition_%(jsname)s(mouseEvent.latLng);
        });

        google.maps.event.addListener(map_%(jsname)s, 'click', function(mouseEvent){
            marker.setPosition(mouseEvent.latLng);
            savePosition_%(jsname)s(mouseEvent.latLng);
        });

    }
    
    $(function() {
        load_%(jsname)s();
    });

//-->
</script>
        ''' % dict(name=name, lat=lat, lng=lng, jsname=jsname)
        html = self.inner_widget.render("%s" % name, "%f,%f" % (lat, lng), dict(id='id_%s' % name))
        html += '<div id="map_%s" style="width: %dpx; height: %dpx"></div>' % (jsname, self.map_width, self.map_height)

        return mark_safe(js + html)

    class Media:
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js',
            'http://maps.google.com/maps/api/js?sensor=false',
        )