# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing M2M table for field dossier on 'Video'
        db.delete_table('medias_video_dossier')

        # Removing M2M table for field dossier on 'Gallery'
        db.delete_table('medias_gallery_dossier')

        # Removing M2M table for field dossier on 'Photo'
        db.delete_table('medias_photo_dossier')

        # Removing M2M table for field dossier on 'Link'
        db.delete_table('medias_link_dossier')

        # Removing M2M table for field dossier on 'Sound'
        db.delete_table('medias_sound_dossier')

        # Removing M2M table for field dossier on 'Map'
        db.delete_table('medias_map_dossier')

        # Removing M2M table for field dossier on 'Book'
        db.delete_table('medias_book_dossier')

        # Adding M2M table for field dossier on 'Media'
        db.create_table('medias_media_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('media', models.ForeignKey(orm['medias.media'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_media_dossier', ['media_id', 'dossier_id'])

        # Removing M2M table for field dossier on 'Text'
        db.delete_table('medias_text_dossier')


    def backwards(self, orm):
        # Adding M2M table for field dossier on 'Video'
        db.create_table('medias_video_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('video', models.ForeignKey(orm['medias.video'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_video_dossier', ['video_id', 'dossier_id'])

        # Adding M2M table for field dossier on 'Gallery'
        db.create_table('medias_gallery_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('gallery', models.ForeignKey(orm['medias.gallery'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_gallery_dossier', ['gallery_id', 'dossier_id'])

        # Adding M2M table for field dossier on 'Photo'
        db.create_table('medias_photo_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('photo', models.ForeignKey(orm['medias.photo'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_photo_dossier', ['photo_id', 'dossier_id'])

        # Adding M2M table for field dossier on 'Link'
        db.create_table('medias_link_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('link', models.ForeignKey(orm['medias.link'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_link_dossier', ['link_id', 'dossier_id'])

        # Adding M2M table for field dossier on 'Sound'
        db.create_table('medias_sound_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('sound', models.ForeignKey(orm['medias.sound'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_sound_dossier', ['sound_id', 'dossier_id'])

        # Adding M2M table for field dossier on 'Map'
        db.create_table('medias_map_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('map', models.ForeignKey(orm['medias.map'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_map_dossier', ['map_id', 'dossier_id'])

        # Adding M2M table for field dossier on 'Book'
        db.create_table('medias_book_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('book', models.ForeignKey(orm['medias.book'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_book_dossier', ['book_id', 'dossier_id'])

        # Removing M2M table for field dossier on 'Media'
        db.delete_table('medias_media_dossier')

        # Adding M2M table for field dossier on 'Text'
        db.create_table('medias_text_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('text', models.ForeignKey(orm['medias.text'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_text_dossier', ['text_id', 'dossier_id'])


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'dossiers.dossier': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Dossier'},
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'dossier_authors'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'}),
            'introduction': ('django.db.models.fields.TextField', [], {}),
            'location': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'pdf': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'medias.book': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Book', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.gallery': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Gallery', '_ormbases': ['medias.Media']},
            'folder': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.image': {
            'Meta': {'ordering': "['order']", 'object_name': 'Image'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': "orm['medias.Gallery']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500', 'null': 'True'}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'medias.link': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Link', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'medias.map': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Map', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'code': ('django.db.models.fields.TextField', [], {}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.media': {
            'Meta': {'object_name': 'Media'},
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'media_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'real_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'medias.photo': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Photo', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'photo': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'})
        },
        'medias.sound': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Sound', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'mp3': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'})
        },
        'medias.text': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Text', '_ormbases': ['medias.Media']},
            'body': ('django.db.models.fields.TextField', [], {}),
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.video': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Video', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'code': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['medias']