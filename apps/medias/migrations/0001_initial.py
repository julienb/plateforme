# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Media'
        db.create_table('medias_media', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('real_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=225)),
            ('status', self.gf('django.db.models.fields.CharField')(default='PUBLISHED', max_length=24)),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
        ))
        db.send_create_signal('medias', ['Media'])

        # Adding model 'Photo'
        db.create_table('medias_photo', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('photo', self.gf('filebrowser.fields.FileBrowseField')(max_length=500)),
            ('copyright', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('caption', self.gf('django.db.models.fields.TextField')()),
            ('latlng', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('medias', ['Photo'])

        # Adding M2M table for field dossier on 'Photo'
        db.create_table('medias_photo_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('photo', models.ForeignKey(orm['medias.photo'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_photo_dossier', ['photo_id', 'dossier_id'])

        # Adding model 'Video'
        db.create_table('medias_video', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('code', self.gf('django.db.models.fields.TextField')()),
            ('copyright', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('caption', self.gf('django.db.models.fields.TextField')()),
            ('latlng', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('medias', ['Video'])

        # Adding M2M table for field dossier on 'Video'
        db.create_table('medias_video_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('video', models.ForeignKey(orm['medias.video'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_video_dossier', ['video_id', 'dossier_id'])

        # Adding model 'Sound'
        db.create_table('medias_sound', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('mp3', self.gf('filebrowser.fields.FileBrowseField')(max_length=500)),
            ('copyright', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('caption', self.gf('django.db.models.fields.TextField')()),
            ('latlng', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('medias', ['Sound'])

        # Adding M2M table for field dossier on 'Sound'
        db.create_table('medias_sound_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('sound', models.ForeignKey(orm['medias.sound'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_sound_dossier', ['sound_id', 'dossier_id'])

        # Adding model 'Map'
        db.create_table('medias_map', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('code', self.gf('django.db.models.fields.TextField')()),
            ('caption', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('medias', ['Map'])

        # Adding M2M table for field dossier on 'Map'
        db.create_table('medias_map_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('map', models.ForeignKey(orm['medias.map'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_map_dossier', ['map_id', 'dossier_id'])

        # Adding model 'Text'
        db.create_table('medias_text', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('copyright', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('caption', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('medias', ['Text'])

        # Adding M2M table for field dossier on 'Text'
        db.create_table('medias_text_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('text', models.ForeignKey(orm['medias.text'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_text_dossier', ['text_id', 'dossier_id'])

        # Adding model 'Link'
        db.create_table('medias_link', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('caption', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('medias', ['Link'])

        # Adding M2M table for field dossier on 'Link'
        db.create_table('medias_link_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('link', models.ForeignKey(orm['medias.link'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_link_dossier', ['link_id', 'dossier_id'])

        # Adding model 'Book'
        db.create_table('medias_book', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('copyright', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('caption', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('medias', ['Book'])

        # Adding M2M table for field dossier on 'Book'
        db.create_table('medias_book_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('book', models.ForeignKey(orm['medias.book'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_book_dossier', ['book_id', 'dossier_id'])

        # Adding model 'Gallery'
        db.create_table('medias_gallery', (
            ('media_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['medias.Media'], unique=True, primary_key=True)),
            ('folder', self.gf('filebrowser.fields.FileBrowseField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal('medias', ['Gallery'])

        # Adding M2M table for field dossier on 'Gallery'
        db.create_table('medias_gallery_dossier', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('gallery', models.ForeignKey(orm['medias.gallery'], null=False)),
            ('dossier', models.ForeignKey(orm['dossiers.dossier'], null=False))
        ))
        db.create_unique('medias_gallery_dossier', ['gallery_id', 'dossier_id'])

        # Adding model 'Image'
        db.create_table('medias_image', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=225)),
            ('gallery', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['medias.Gallery'])),
            ('latlng', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('order', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('image', self.gf('filebrowser.fields.FileBrowseField')(max_length=500, null=True)),
        ))
        db.send_create_signal('medias', ['Image'])


    def backwards(self, orm):
        # Deleting model 'Media'
        db.delete_table('medias_media')

        # Deleting model 'Photo'
        db.delete_table('medias_photo')

        # Removing M2M table for field dossier on 'Photo'
        db.delete_table('medias_photo_dossier')

        # Deleting model 'Video'
        db.delete_table('medias_video')

        # Removing M2M table for field dossier on 'Video'
        db.delete_table('medias_video_dossier')

        # Deleting model 'Sound'
        db.delete_table('medias_sound')

        # Removing M2M table for field dossier on 'Sound'
        db.delete_table('medias_sound_dossier')

        # Deleting model 'Map'
        db.delete_table('medias_map')

        # Removing M2M table for field dossier on 'Map'
        db.delete_table('medias_map_dossier')

        # Deleting model 'Text'
        db.delete_table('medias_text')

        # Removing M2M table for field dossier on 'Text'
        db.delete_table('medias_text_dossier')

        # Deleting model 'Link'
        db.delete_table('medias_link')

        # Removing M2M table for field dossier on 'Link'
        db.delete_table('medias_link_dossier')

        # Deleting model 'Book'
        db.delete_table('medias_book')

        # Removing M2M table for field dossier on 'Book'
        db.delete_table('medias_book_dossier')

        # Deleting model 'Gallery'
        db.delete_table('medias_gallery')

        # Removing M2M table for field dossier on 'Gallery'
        db.delete_table('medias_gallery_dossier')

        # Deleting model 'Image'
        db.delete_table('medias_image')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'dossiers.dossier': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Dossier'},
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'dossier_authors'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'}),
            'introduction': ('django.db.models.fields.TextField', [], {}),
            'location': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'pdf': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'medias.book': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Book', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'book_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.gallery': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Gallery', '_ormbases': ['medias.Media']},
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'gallery_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'folder': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.image': {
            'Meta': {'ordering': "['order']", 'object_name': 'Image'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': "orm['medias.Gallery']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500', 'null': 'True'}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'medias.link': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Link', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'link_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'medias.map': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Map', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'code': ('django.db.models.fields.TextField', [], {}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'map_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.media': {
            'Meta': {'object_name': 'Media'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'real_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'medias.photo': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Photo', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'photo_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'photo': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'})
        },
        'medias.sound': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Sound', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'sound_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'mp3': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'})
        },
        'medias.text': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Text', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'text_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'medias.video': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Video', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'code': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'video_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_tagged_items'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['medias']