# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Text.text'
        db.delete_column('medias_text', 'text')

        # Adding field 'Text.body'
        db.add_column('medias_text', 'body',
                      self.gf('django.db.models.fields.TextField')(default='a'),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Text.text'
        raise RuntimeError("Cannot reverse this migration. 'Text.text' and its values cannot be restored.")
        # Deleting field 'Text.body'
        db.delete_column('medias_text', 'body')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'dossiers.dossier': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Dossier'},
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'dossier_authors'", 'symmetrical': 'False', 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'}),
            'introduction': ('django.db.models.fields.TextField', [], {}),
            'location': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'pdf': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'medias.book': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Book', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'book_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.gallery': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Gallery', '_ormbases': ['medias.Media']},
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'gallery_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'folder': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.image': {
            'Meta': {'ordering': "['order']", 'object_name': 'Image'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': "orm['medias.Gallery']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500', 'null': 'True'}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'medias.link': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Link', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'link_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'medias.map': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Map', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'code': ('django.db.models.fields.TextField', [], {}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'map_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.media': {
            'Meta': {'object_name': 'Media'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'real_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLISHED'", 'max_length': '24'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '225'})
        },
        'medias.photo': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Photo', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'photo_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'photo': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'})
        },
        'medias.sound': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Sound', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'sound_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'}),
            'mp3': ('filebrowser.fields.FileBrowseField', [], {'max_length': '500'})
        },
        'medias.text': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Text', '_ormbases': ['medias.Media']},
            'body': ('django.db.models.fields.TextField', [], {}),
            'caption': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'text_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'medias.video': {
            'Meta': {'ordering': "['-publication_date']", 'object_name': 'Video', '_ormbases': ['medias.Media']},
            'caption': ('django.db.models.fields.TextField', [], {}),
            'code': ('django.db.models.fields.TextField', [], {}),
            'copyright': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'dossier': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'video_dossier'", 'symmetrical': 'False', 'to': "orm['dossiers.Dossier']"}),
            'latlng': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'media_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['medias.Media']", 'unique': 'True', 'primary_key': 'True'})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_tagged_items'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['medias']