#-*- coding: utf-8 -*-

from django.views.generic import DetailView

from dossiers.models import PublicDossier
from medias.models import PublicMedia

# event detail view    
class MediaDetail(DetailView):
    model = PublicMedia
    template_name = "media.html"
    
    def get_context_data(self, **kwargs):
        context = super(MediaDetail, self).get_context_data(**kwargs)
        context['medias'] = PublicMedia.objects.all()
        context['dossiers'] = PublicDossier.objects.all()

        return context
