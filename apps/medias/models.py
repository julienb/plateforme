#-*- coding: utf-8 -*- 

from django.db import models
from django.db.models import permalink
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core import urlresolvers


from model_utils.managers import QueryManager, InheritanceManager
from model_utils import Choices
from model_utils.models import InheritanceCastModel

from ag.utils.slughifi import slughifi

from filebrowser.fields import FileBrowseField
from datetime import datetime
#from time import strftime

from dossiers.models import Dossier
import os

from tags.models import TaggedContent
from taggit.managers import TaggableManager


class Media(InheritanceCastModel):
    title = models.CharField(max_length=225, verbose_name='Titre')
    status = models.CharField(choices=settings.EDITORIAL_STATUS, max_length=24, default='PUBLISHED', verbose_name = 'Statut', help_text='Publié, brouillon…')
    publication_date = models.DateTimeField('Date', default=datetime.now, blank=True)
    dossier = models.ManyToManyField(Dossier, verbose_name=u'Dossier', related_name='media_dossier') 

    def get_tpl_name(self):
        content_type = ContentType.objects.get_for_id(self.real_type_id) 
        return "medias/%s.html" % content_type.model

    def get_model_name(self):
        content_type = ContentType.objects.get_for_id(self.real_type_id) 
        return content_type.model
    
    def get_admin_url(self):
        content_type = ContentType.objects.get_for_id(self.real_type_id) 
        object_admin_url = urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return object_admin_url

    @permalink
    def get_absolute_url(self):
        print self.real_type_id
        return ('media_detail_view',(),{'type': self.real_type_id, 'pk':self.id, 'slug':slughifi(self.title)})


    # TODO is_restricted = models.Boolean()

    tags = TaggableManager(blank=True, through=TaggedContent)    

class PublicMedia(Media):
    objects = QueryManager(status='PUBLISHED', publication_date__lte=datetime.now).order_by('publication_date')
    class Meta:
        proxy = True               



class Photo(Media):

    photo = FileBrowseField("Photo", max_length=500,  extensions=[".jpg",".png"], help_text='Uploader et sélectionner une photo de grande taille (> 800px)')
    
    copyright = models.CharField(max_length=128, verbose_name='Crédits', help_text='Par exemple : « Photo de Loïc Morizur »')    
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif du média. Maximum 500 caractères')
    latlng = models.CharField(verbose_name ='Latitude / longitude', blank=True, max_length=100)
        
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Photo"
        verbose_name_plural = "Photos"
    
    def __unicode__(self):
        return self.title
    
class Video(Media):
    
    code = models.TextField(verbose_name='Code', help_text='Code d’intégration de la vidéo déposée sur Vimeo')
    
    copyright = models.CharField(max_length=128, verbose_name='Crédits', help_text='Par exemple : « Vidéo de Pauline Terne »')    
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif du média. Maximum 500 caractères')
    latlng = models.CharField(verbose_name ='Latitude / longitude', blank=True, max_length=100)

    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Vidéo"
        verbose_name_plural = "Vidéos"
    
    def __unicode__(self):
        return self.title            



class Sound(Media):
    
    mp3 = FileBrowseField("Mp3", max_length=500,  extensions=[".mp3",".ogg"], help_text='Uploader et sélectionner un fichier mp3')
    
    copyright = models.CharField(max_length=128, verbose_name='Crédits', help_text='Par exemple : « Enregistrement d’Amélie Lasserre »')    
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif du média. Maximum 500 caractères')
    latlng = models.CharField(verbose_name ='Latitude / longitude', blank=True, max_length=100)
    
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Audio"
        verbose_name_plural = "Audio"
    
    def __unicode__(self):
        return self.title


class Map(Media):
    
    code = models.TextField(verbose_name='Code', help_text='Code d’intégration de la carte ou streetmap')
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif du média. Maximum 500 caractères')
    
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Carte"
        verbose_name_plural = "Cartes"
    
    def __unicode__(self):
        return self.title


class Text(Media):
    
    body = models.TextField(verbose_name='Texte', help_text='')
    copyright = models.CharField(max_length=128, verbose_name='Crédits', help_text='Par exemple : « Texte de Fanny Desmarais »')    
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif du média. Maximum 500 caractères')

    
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Texte"
        verbose_name_plural = "Textes"
    
    def __unicode__(self):
        return self.title



class Link(Media):
    
    url = models.URLField(verbose_name='URL', help_text='Adresse complète (absolue) de la page web référencée')
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif du site ou de la page web. Maximum 500 caractères')
    
    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Lien web"
        verbose_name_plural = "Liens web"
    
    def __unicode__(self):
        return self.title



class Book(Media):
    
    copyright = models.CharField(max_length=128, verbose_name='Auteur(s)', help_text='Auteur(s) de l’ouvrage ou de l’article.')    
    caption = models.TextField(verbose_name='Légende', help_text='Descriptif. Maximum 500 caractères')
    

    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Biblio."
        verbose_name_plural = "Biblio."
    
    def __unicode__(self):
        return self.title
    

class Gallery(Media):

    folder = FileBrowseField(max_length=500, verbose_name='Dossier', format='folder', null=True, blank=True,  help_text='Sélectionner un dossier pour automatiser la création des images. Tous les fichiers contenus dans le dossier généreront les images de la galerie. <strong>L’automatisation n’est active que si la galerie est vide.</strong>')
    

    def get_first_image(self):
        return self.images.all()[0]

    class Meta:
        ordering = ['-publication_date',]
        verbose_name = "Galerie"
        verbose_name_plural = "Galeries"
    
    def __unicode__(self):
        if self.status == 'DRAFT':
            return "%s (brouillon)" % self.title
        else:
            return self.title
    
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains",)

    def my_containsAny(str, set):
        for c in set:
            if c in str: return 1;
        return 0;

    def my_containsAll(str, set):
        for c in set:
            if c not in str: return 0;
        return 1;

    def save(self, *args, **kargs):
        
        if self.folder and self.images.all().count()==0 and self.folder.is_folder :

            gallery = self
            super(Gallery, self).save()
        
            abspath = "%s%s" % (settings.MEDIA_ROOT, self.folder.path)
            basedir = self.folder.path
            img_list = os.listdir(abspath)
            gallery = self
            index = 0
            for image in img_list:
                if not 'thumbnail' in image:
                    image_path = "%s/%s" % (basedir, image)
                    title = image.replace('-', " ").replace('_', " ")[:-4]
                    i = Image(title=title, gallery = gallery, order = index, image = image_path)
                    i.save()
                    index += 1

        super(Gallery, self).save(*args, **kargs)

    
class PublicGallery(Gallery):
    objects = QueryManager(status='PUBLISHED', publication_date__lte=datetime.now).order_by('-publication_date')
    class Meta:
        proxy=True
    
    
class Image(models.Model):
    title = models.CharField(max_length=225, verbose_name='Titre', help_text="Titre / légende et crédits de l'image")
    gallery = models.ForeignKey('Gallery', related_name='images')    
    latlng = models.CharField(verbose_name ='Latitude / longitude', blank=True, max_length=100)
    order = models.PositiveSmallIntegerField(verbose_name='Ordre', help_text='')
    image = FileBrowseField(max_length=500, verbose_name='Image', format='image', null=True, help_text='jpg ou png')
    
    def __unicode__(self):
        return self.title
        
    class Meta:
        ordering = ['order']
        verbose_name = "Image"
        verbose_name_plural = "Images"

    def get_next(self):
        """
        si queryset a des resultat => renvoyer le premier, sinon renvoyer None
        """
        qs = Image.objects.filter(gallery=self.gallery, order__gt=self.order).order_by("order")
        if qs.count()>0 :
            return qs[0]
        
        else :
            return None

    def get_prev(self):
        
        qs = Image.objects.filter(gallery=self.gallery, order__lt=self.order).order_by("-order")
        if qs.count()>0 :
            return qs[0]

        else :
            return None
        