#-*- coding: utf-8 -*-

from django.conf.urls import patterns
from models import Media, PublicMedia
from views import *

urlpatterns = patterns('',
   (r'^(?P<type>\d+)/(?P<pk>\d+)/(?P<slug>\S+)$', MediaDetail.as_view(), {}, "media_detail_view"),
                       
)