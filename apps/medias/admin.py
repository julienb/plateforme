#-*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from medias.models import Photo, Video, Sound, Text, Link, Book, Map, Gallery, Image
from medias.forms import PhotoForm, VideoForm, SoundForm, ImageForm, InlineImageForm
from sorl.thumbnail.admin import AdminImageMixin
from django.db import models
from django.forms.widgets import HiddenInput


from medias.widgets import LocationWidget
 

class ImageInline(AdminImageMixin, admin.TabularInline):
    model = Image
    extra = 0
    sortable_field_name = "order"
    fields = ('title', 'latlng', 'image', 'order')
    form = InlineImageForm
    formfield_overrides = {
        models.PositiveSmallIntegerField: {'widget': HiddenInput},        
    }
    
    
class GalleryAdmin(admin.ModelAdmin):   
    class Media:
        js = ("admin/hide_order.js",)
        
    list_display = ('title', 'status', 'publication_date', )
    ordering = ('-publication_date',)
    search_fields = ('title',)
    list_editable = ('status',)
    fieldsets = (('', {
                 'fields':('title','folder', 'tags','dossier')
                 }),
                 
                 ('Avancé', {
                            'classes': ('grp-collapse grp-closed',),
                            'fields':('status', 'publication_date')
                 })
                 )
    
    inlines = [ImageInline]

    
    
admin.site.register(Gallery,GalleryAdmin)



class PhotoAdmin(admin.ModelAdmin):   
    form = PhotoForm
    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title', 'photo', 'copyright','caption','latlng','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    

    
    
admin.site.register(Photo, PhotoAdmin)



class VideoAdmin(admin.ModelAdmin):   
    form = VideoForm
    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title','code',  'copyright','caption','latlng','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    

    
    
admin.site.register(Video, VideoAdmin)




class SoundAdmin(admin.ModelAdmin):   
    form = SoundForm
    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title', 'mp3', 'copyright','caption','latlng','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    
    
    
admin.site.register(Sound, SoundAdmin)



class MapAdmin(admin.ModelAdmin):   
    
    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title','code', 'caption','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    

    
    
admin.site.register(Map, MapAdmin)



class TextAdmin(admin.ModelAdmin):   
    class Media:
        js = (
              settings.STATIC_URL+'grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
              settings.STATIC_URL+'tiny_mce_setup/tiny_mce_setup.js',
              )

    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title','body',  'copyright','caption','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    

    
    
admin.site.register(Text, TextAdmin)


class LinkAdmin(admin.ModelAdmin):   
    
    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title','url','caption','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    
    
    

admin.site.register(Link, LinkAdmin)




class BookAdmin(admin.ModelAdmin):   
    
    list_display = ('id', 'title', 'status', 'publication_date')
    fieldsets = (
        ('', {
            'fields':('title', 'copyright', 'caption','tags', 'dossier')
            }),
        ('Statut', {
            'classes': ('grp-collapse grp-closed',),
            'fields':( 'status', 'publication_date', )
            }),
        )

    search_fields = ('title',)    
    
    
admin.site.register(Book, BookAdmin)



    