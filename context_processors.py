# -*- coding: utf-8 -*-
from django.conf import settings # import the settings file



def common(context):
    # return the value you want as a dictionnary. you may add multiple values in there.
    

    return {
        'DOMAIN': settings.DOMAIN, 
        'SITE_NAME':'Les écritures du patrimoine – enquêter l’écrit en situation.',
        'DEBUG': settings.DEBUG,
        'MAGIC_URL': settings.MAGIC_URL,
        
    }

def user_context(request):

    if request.user.is_authenticated():
        is_admin = is_local_admin(request.user)
    else:
        is_admin = False

    return {
        'is_local_admin': is_admin,

    }    

def get_css(request):

    is_admin =  request.user.is_authenticated()
        
    print "%s %s" % (is_admin, request.user.groups)

    q = request.GET.get('css', '')
    
    css='main'
    
    if q == 'loic':
        css = 'loic'
    if q == 'marine':                
        css = 'marine'
    if  q == 'sebastien':
        css = 'sebastien'

    
    css = 'http://plateforme.accentgrave.net/statics/css/%s.css' % css

    return {
       'CSS': css,
       'IS_ADMIN':is_admin,
       'current_path': request.get_full_path()
     }