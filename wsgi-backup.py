"""
WSGI config for reels project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys
import site

site.addsitedir('/var/www/vhosts/recherche.esapyrenees.fr/plateforme/lib/python2.7/site-packages/')

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings_production_esa")

activate_env=os.path.expanduser("/var/www/vhosts/recherche.esapyrenees.fr/plateforme/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))	


from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
