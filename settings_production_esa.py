from settings import *

########################################################################################## DB
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'accentgrave_plateforme',
        'USER': '27990_plateforme',
        'PASSWORD': '8vPlyyx4',
        'HOST': 'mysql.accentgrave.alwaysdata.net',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'plateforme_cache',
    }
}



DOMAIN = 'http://recherche.esapyrenees.fr/ecritures/'

MAGIC_URL = DOMAIN + MEDIA_URL + 'magick/img/'

DEBUG = False
THUMBNAIL_DEBUG = DEBUG
