# -*- coding: utf-8 -*-
from django.views.generic import TemplateView, ListView
from django.conf import settings

from datetime import datetime
import random
from random import shuffle
import string

import os

from dossiers.models import PublicDossier
from medias.models import PublicMedia
from articles.models import PublicArticle

class Home(ListView):
    model = PublicDossier
    template_name = "home.html"

    #print PublicDossier.objects.all()
    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)

        context['dossiers'] = PublicDossier.objects.all()
        context['photos'] = PublicMedia.objects.filter(real_type__model__in=['Photo'])
        context['intro'] = PublicArticle.objects.get(title__icontains='Introduction')    
        context['articles'] = PublicArticle.objects.exclude(title__icontains='Introduction')    
        
        return context