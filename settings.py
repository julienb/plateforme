# -*- coding: utf-8 -*-

import os, sys
from path import path
from django.conf.urls import url


########################################################################################## Path

PROJECT_ROOT = path(__file__).abspath().dirname()
SITE_ROOT = PROJECT_ROOT.dirname()

sys.path.append(SITE_ROOT)
sys.path.append(PROJECT_ROOT / 'apps')
sys.path.append(PROJECT_ROOT / 'libs')

########################################################################################## Debug
DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

########################################################################################## Users
ADMINS = (
    ('Julien Bidoret', 'julienbidoret@gmail.com'),
)
MANAGERS = ADMINS


########################################################################################## DB


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'plateforme',
        'USER': '27990_plateforme',
        'PASSWORD': '8vPlyyx4',
        'HOST': 'localhost',
        'PORT': '',
    }
}    

########################################################################################## Cache


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}


########################################################################################## Time zone and language
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'
USE_I18N = True
USE_L10N = True
USE_TZ = False



########################################################################################## Site misc
# Site ID
SITE_ID = 1

# Make this unique, and don't share it with anybody.
SECRET_KEY = '_^jmavt#8)y&amp;e%)!8*@5+d-u80g#6p#foe(m-ah-bon?dsig)xfrfrzz98(2+s16'

ROOT_URLCONF = 'urls'

INTERNAL_IPS = ('127.0.0.1',)

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'


########################################################################################## Media & static roots & urls
# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT =  PROJECT_ROOT + '/public/medias/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/medias/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT =  PROJECT_ROOT + '/public/statics/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/statics/'

# Additional locations of static files
STATICFILES_DIRS = (PROJECT_ROOT + '/static/',)

for root, dirs, files in os.walk(PROJECT_ROOT):
    if 'static' in dirs: STATICFILES_DIRS += (os.path.join(root, 'static'),)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)



########################################################################################## Templates

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)
TEMPLATE_DIRS = (
                 os.path.join(PROJECT_ROOT, "templates"),
                 )

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'context_processors.common',
    'context_processors.get_css',
)


########################################################################################## Middlewares

MIDDLEWARE_CLASSES = (
    # cache
    #'django.middleware.cache.UpdateCacheMiddleware',
    
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # flatpages off
    #'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # custom pages on
    #'pages.middleware.PageFallbackMiddleware',
    #'pagination.middleware.PaginationMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # cache
    #'django.middleware.cache.FetchFromCacheMiddleware',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #'django.contrib.flatpages',
    
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    'django.contrib.admin',

    'sorl.thumbnail',
    'south',

    'typogrify',
    'ag',
    
    'taggit',
    'taggit_templatetags',

    'dossiers',
    'articles',
    'medias',
    'tags'
    
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
              
)

########################################################################################## Grappelli
GRAPPELLI_ADMIN_TITLE = "Écritures / patrimoine - Administration "
GRAPPELLI_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
# ADMIN_MEDIA_PREFIX = STATIC_URL + "grappelli/"


########################################################################################## Send emails

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'kronstadtforever@gmail.com'
EMAIL_HOST_PASSWORD = 'ovbjm88Gg'
EMAIL_PORT = 587

########################################################################################## Config
DOMAIN = 'http://192.168.1.83:8000'
EDITORIAL_STATUS = (('DRAFT', 'Brouillon'), ('PUBLISHED','Publié'), ('DELETED', 'Supprimé'),)

#MAGIC_URL = 'http://accentgrave.net/exp/mono/magick/img/'
MAGIC_URL = 'http://plateforme.accentgrave.net/medias/magick/img/'

TAGGIT_TAGCLOUD_MIN = 1
TAGGIT_TAGCLOUD_MAX = 10

########################################################################################## Filebrowser
TINYMCE_FILEBROWSER = True
FILEBROWSER_VERSIONS = ({
    'admin_thumbnail': {'verbose_name': 'Admin', 'width': 60, 'height': 60, 'opts': 'crop'},
    'small': {'verbose_name': 'Petite', 'width': 260, 'height': '', 'opts': ''},
    'medium': {'verbose_name': 'Moyenne', 'width': 500, 'height': '', 'opts': ''},
    'big': {'verbose_name': 'Grande', 'width': 660, 'height': '', 'opts': ''},
})

FILEBROWSER_ADMIN_VERSIONS = ('small', 'medium', 'big')
FILEBROWSER_ADMIN_THUMBNAIL = 'admin_thumbnail'
FILEBROWSER_CONVERT_FILENAME = True
FILEBROWSER_NORMALIZE_FILENAME = True

FILEBROWSER_EXTENSIONS = ({
    'Folder': [''],
    'Image': ['.jpg','.jpeg','.gif','.png'],
    'Document': ['.pdf','.doc','.rtf','.txt','.xls','.csv'],
    'Video': ['.mov','.wmv','.mpeg','.mpg','.avi','.rm'],
    'Audio': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p']
})

FILEBROWSER_SELECT_FORMATS = ({
    'file': ['Folder','Image','Document','Video','Audio'],
    'image': ['Image'],
    'folder':['Folder'],
    'document': ['Document'],
    'media': ['Video','Audio'],
})

########################################################################################## Logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
