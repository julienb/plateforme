# -*- coding: utf-8 -*-
from django import template                  
from datetime import datetime
from time import strftime

import re

from django.conf import settings
from django.core.urlresolvers import resolve
from django.template import Library, Node, VariableDoesNotExist

import base64

from sorl.thumbnail import get_thumbnail

register = template.Library()

@register.simple_tag
def active(request, pattern):
    import re
    if re.search(pattern, request.path):
        return 'active'
    return ''

@register.filter
def verbose(value, arg):
    """
    Usage:
        {{ name|verbose:"Hello %s this is a dummy text" }}
        {{ image|verbose:"<img src=\"%s\" />" }}
    """
    try:    
        if not value:
            return ''
        return arg % value
    except Exception:
        return str(value)

@register.filter
def extract_domain(url):
    try:
        url = url.replace('http://', '')
        url = url.replace('www.', '')
        url = url.split('/')[0]
        return url
    except:
        print 'Exception in extract_domain', text
        return ''

@register.filter
def wrap_meta(t):

    t = t.replace('<span>', '<span><span>' )
    t = t.replace('<span class="meta">', '<span class="meta"><span>' )
    t = t.replace('</span>', '</span></span>' )
    
    return t



@register.filter
def absolute_url(url):
    try:
        if not re.findall('http://', url):
            url = settings.DOMAIN + url
        return url
    except:
        print 'Exception in absolute_url', text
        return ''
      

@register.filter('read_more')
def read_more(t):
    pos = t.find('<!--more-->')
    if pos == -1:
        return t
    else:
        return '%s <a href="#more">+</a><div class="more">%s</div>' % (t[:pos], t[pos:])

"""

def parseMedia(media): 
    media_att = re.findall('([a-z]*)="([^"]*)',media) 
    for att in media_att:  
        if att[0] == 'class':  
            mediaid = att[1] 


@register.filter('media_in_text')
    liste = re.findall('<!--media (?P<id>\d)-->',t) 
    for media in liste:  
        print id
        
        try:
            data = parseImg(img)
            media = 
            
        
            try:
                p = Poll.objects.get(pk=poll_id)
            except Poll.DoesNotExist:
                raise Http404
            return render_to_response('polls/detail.html', {'poll': p}) 

        except Exception,e:
            print 'ParseMediaInText Exception : ', e
            pass
            
    return t
"""         


def parseImg(img): 
    img_att = re.findall('([a-z]*)="([^"]*)',img) 
    
    img_class = '' 
    img_title = '' 
    img_alt = '' 
    img_src = '' 
    mediasrc = '' 
    img_longdesc = ''
    img_data = None
    #img_data['src'] = None

    for att in img_att:  
        if att[0] == 'class':  
            img_class = att[1] 
        elif att[0] == 'height':  
            img_height = att[1]  
        elif att[0] == 'width':  
            img_width = att[1]  
        elif att[0] == 'title':  
            img_title = att[1] 
        elif att[0] == 'alt':  
            img_alt = att[1]  
        elif att[0] == 'src':  
            img_data = dict()
            img_src = att[1]   
            # remove file extension
            # split the string at the last '_'
            # find the last part : big, medium, small  
            img_size = img_src[:-4].rsplit('_', 1)[1]
            # create a big thumbnail of the image for auto linking
            img_originale = img_src.replace(img_size, '')
            img_originale = img_originale[:-5] +  img_originale[-4:] 
            img_url = img_originale.replace(settings.MEDIA_URL, '')
            im = get_thumbnail(img_url, '800x800', upscale=False, quality=80)
            big_url = im.url

            mediasrc = img_src.replace("medias",'')
            
        elif att[0] == 'longdesc':  
            img_longdesc = att[1] 

    img_data.update({ 
        "class":img_class,
        "title":img_title,
        "src":img_src,
        "alt":img_alt, 
        "longdesc":img_longdesc,
        "height":img_height,
        "width":img_width,
        "img_size": img_size,
        "big_url": big_url,
        "magick": settings.MAGIC_URL,
        "mediasrc":mediasrc
    })
    if img_data is None:
        raise Exception('Can\'t parse image')
    
    return img_data

@register.filter
def parseImgInText(t):
    
    liste = re.findall('<img (?P<all>[^>]+)>',t) 
    for img in liste:    
        try:
            data = parseImg(img)

            t = t.replace('<img %s>' % img, '<a href="%(big_url)s" class="fancybox" title="%(title)s"><span class="%(class)s %(img_size)s clip-image clip-%(img_size)s"><img src="%(src)s" height="%(height)s" width="%(width)s" alt="%(alt)s" />\
                <img src="%(magick)s120/blue/%(width)s.0/%(mediasrc)s"></span></a>'  % data )

        except Exception,e:
            print 'ParseImgInText Exception : ', e
            pass
    return t


@register.tag(name="switch")
def do_switch(parser, token):
    """
    The ``{% switch %}`` tag compares a variable against one or more values in
    ``{% case %}`` tags, and outputs the contents of the matching block.  An
    optional ``{% else %}`` tag sets off the default output if no matches
    could be found::

        {% switch result_count %}
            {% case 0 %}
                There are no search results.
            {% case 1 %}
                There is one search result.
            {% else %}
                Jackpot! Your search found {{ result_count }} results.
        {% endswitch %}

    Each ``{% case %}`` tag can take multiple values to compare the variable
    against::

        {% switch username %}
            {% case "Jim" "Bob" "Joe" %}
                Me old mate {{ username }}! How ya doin?
            {% else %}
                Hello {{ username }}
        {% endswitch %}
    """
    bits = token.contents.split()
    tag_name = bits[0]
    if len(bits) != 2:
        raise template.TemplateSyntaxError("'%s' tag requires one argument" % tag_name)
    variable = parser.compile_filter(bits[1])

    class BlockTagList(object):
        # This is a bit of a hack, as it embeds knowledge of the behaviour
        # of Parser.parse() relating to the "parse_until" argument.
        def __init__(self, *names):
            self.names = set(names)
        def __contains__(self, token_contents):
            name = token_contents.split()[0]
            return name in self.names

    # Skip over everything before the first {% case %} tag
    parser.parse(BlockTagList('case', 'endswitch'))

    cases = []
    token = parser.next_token()
    got_case = False
    got_else = False
    while token.contents != 'endswitch':
        nodelist = parser.parse(BlockTagList('case', 'else', 'endswitch'))
        
        if got_else:
            raise template.TemplateSyntaxError("'else' must be last tag in '%s'." % tag_name)

        contents = token.contents.split()
        token_name, token_args = contents[0], contents[1:]
        
        if token_name == 'case':
            tests = map(parser.compile_filter, token_args)
            case = (tests, nodelist)
            got_case = True
        else:
            # The {% else %} tag
            case = (None, nodelist)
            got_else = True
        cases.append(case)
        token = parser.next_token()

    if not got_case:
        raise template.TemplateSyntaxError("'%s' must have at least one 'case'." % tag_name)

    return SwitchNode(variable, cases)

class SwitchNode(Node):
    def __init__(self, variable, cases):
        self.variable = variable
        self.cases = cases

    def __repr__(self):
        return "<Switch node>"

    def __iter__(self):
        for tests, nodelist in self.cases:
            for node in nodelist:
                yield node

    def get_nodes_by_type(self, nodetype):
        nodes = []
        if isinstance(self, nodetype):
            nodes.append(self)
        for tests, nodelist in self.cases:
            nodes.extend(nodelist.get_nodes_by_type(nodetype))
        return nodes

    def render(self, context):
        try:
            value_missing = False
            value = self.variable.resolve(context, True)
        except VariableDoesNotExist:
            no_value = True
            value_missing = None
        
        for tests, nodelist in self.cases:
            if tests is None:
                return nodelist.render(context)
            elif not value_missing:
                for test in tests:
                    test_value = test.resolve(context, True)
                    if value == test_value:
                        return nodelist.render(context)
        else:
            return ""