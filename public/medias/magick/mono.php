<?php

    $colors = array('blue', 'bluemultiply', 'orange');

    $src = explode('/', $_GET["src"]);

    // tests and variables assignation
    if (is_numeric($src[0])) $uid = $src[0];
    else die($src[0] . ' is not a valid uid');

    if (in_array($src[1], $colors)) $color = $src[1];
    else die($src[1] . ' is not a valid color');

    if (is_numeric($src[2])) {
        $dimensions = explode('.', $src[2]);
        if ($dimensions[0]) $width = $dimensions[0];
        else $width = 'auto';
        if ($dimensions[1]) $height = $dimensions[1];
        else $height = 'auto';
    } else { 
        die($src[2] . ' is not valid as width x height');
    }


    $path = implode("/", array_slice($src, 3));
    $file = './../' . htmlspecialchars($path, ENT_QUOTES, 'UTF-8');
    if (!file_exists($file)) {
        die('No image');
    } else {
        init();
    }
    




    function init(){

        global $debug, $src;

        // debug
        if($_GET["debug"]=="True"){
            $debug=true;
        } else {
            $debug=false;
        }

        // buil dest directory name with uid and color
        $destdir = './img/' . implode("/", array_slice($src, 0, 3));


        // if $destdir does’nt exist, create it, then creat first image
        if (!is_dir($destdir)) {
            mkdir($destdir, 0777, true);
        }

        if($debug==true){
            // remove previously calculated files
            exec ("rm -f " . $destdir . "/*");
        }
        createImage($destdir);

    }



    function createImage($destdir){

        global $debug, $uid, $color, $file, $width, $height;

        // ------------------------------------------------------------------------------------------------ init
        
        //  location of imagemagick's convert utility
        $convert = '/usr/bin/convert';

        // image properties from file
        list($o_width, $o_height, $o_type, $o_attr) = getimagesize($file);

        if ($width == 0) $width = $o_width;
        
        if ($height == 0) $height = $o_height;
        
        // ------------------------------------------------------------------------------------------------ Caching
        // retrieve the file name (end)
        $cache = end(split('/',$file));
        $cache = $destdir . '/' . $cache;
        $tmp = $cache . 'tmp';
        
        $resize = $width . 'x' . $height;
        
        

        switch ($color) {
            case 'blue':
                // création du canvas
                exec ($convert . " -size " . $resize . " xc:'#111138' " . $cache); 
                // manipulation de l’image photo source
                exec ($convert . " " . $file . " -resize '". $resize ."^' -gravity Center -crop ". $resize ."+0+0 +repage -colorspace Gray -fill black -colorize 10% +level-colors '#000000', -ordered-dither h4x4a " . $tmp);
                exec ($convert  . " -geometry +0+0 " . $cache . " " . $tmp . " -composite  " . $cache);
                break;   

            case 'intext':
                // création du canvas
                exec ($convert . " -size " . $resize . " xc:'#111138' " . $cache); 
                // manipulation de l’image photo source
                exec ($convert . " " . $file . " -resize '". $resize ."^' -gravity Center -crop ". $resize ."+0+0 +repage -colorspace Gray -fill black -colorize 10% +level-colors '#000000', -ordered-dither h4x4a " . $tmp);
                exec ($convert  . " -geometry +0+0 " . $cache . " " . $tmp . " -composite  " . $cache);
                break;     

            case 'bluue':
                // création du canvas
                exec ($convert . " -size " . $resize . " xc:'#111138' " . $cache); 
                // manipulation de l’image photo source
                exec ($convert . " " . $file . " -resize '". $resize ."^' -gravity Center -crop ". $resize ."+0+0 +repage -colorspace Gray -fill black -colorize 10% " . $tmp);
                exec ($convert  . " -geometry +0+0 " . $cache . " " . $tmp . " -compose Multiply -composite  " . $cache);
                break;                
        }    

        // suppression des images temporaires
        exec ("rm -f " . $tmp);

        // if there is still no image, die
        if (!file_exists($cache)) {
            die('ERROR: Image conversion failed.');
        } else {
            serveImage($cache);
        }
    }




    function serveImage($cache){
        // get image data for use in http-headers
        
        $imginfo = getimagesize($cache);
        $content_length = filesize($cache);
        $last_modified = gmdate('D, d M Y H:i:s', filemtime($cache)) . ' GMT';

        // array of getimagesize() mime types
        $getimagesize_mime = array(1 => 'image/gif', 2 => 'image/jpeg', 3 => 'image/png', 4 => 'application/x-shockwave-flash', 5 => 'image/psd', 6 => 'image/bmp', 7 => 'image/tiff', 8 => 'image/tiff', 9 => 'image/jpeg', 13 => 'application/x-shockwave-flash', 14 => 'image/iff');

        // did the browser send an if-modified-since request?
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
            // parse header
            $if_modified_since = preg_replace('/;.*$/', '', $_SERVER['HTTP_IF_MODIFIED_SINCE']);

            if ($if_modified_since == $last_modified) {
                // the browser's cache is still up to date
                header("HTTP/1.0 304 Not Modified");
                header("Cache-Control: max-age=86400, must-revalidate");
                exit ;
            }
        }

        // send other headers
        header('Cache-Control: max-age=86400, must-revalidate');
        header('Content-Length: ' . $content_length);
        header('Last-Modified: ' . $last_modified);
        if (isset($getimagesize_mime[$imginfo[2]])) {
            header('Content-Type: ' . $getimagesize_mime[$imginfo[2]]);
        } else {
            // send generic header
            header('Content-Type: application/octet-stream');
        }

        // and finally, send the image
        readfile($cache);
    }

?>